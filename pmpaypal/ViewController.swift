//
//  ViewController.swift
//  pmpaypal
//
//  Created by Cristobal Nuñez on 16/1/17.
//  Copyright © 2017 Iliux. All rights reserved.
//

import UIKit
import WebKit
import AVFoundation

class ViewController: UIViewController, WKScriptMessageHandler, AVCaptureMetadataOutputObjectsDelegate {
    // for webview
    var webView:WKWebView!
    
    //for code bar
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var codeFrameView:UIView?
    var cancelButton:UIButton?
    let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                              AVMetadataObject.ObjectType.code39,
                              AVMetadataObject.ObjectType.code39Mod43,
                              AVMetadataObject.ObjectType.ean13,
                              AVMetadataObject.ObjectType.ean8,
                              AVMetadataObject.ObjectType.code93,
                              AVMetadataObject.ObjectType.code128,
                              AVMetadataObject.ObjectType.pdf417,
                              AVMetadataObject.ObjectType.aztec,
                              AVMetadataObject.ObjectType.interleaved2of5,
                              AVMetadataObject.ObjectType.itf14
                             ]

    // for data
    var charactersLongitude = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startWebView();
    }
    
    func startCamera(){
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter.
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture.
            captureSession?.startRunning()
            
            // Initialize Code Frame to highlight the QR code
            codeFrameView = UIView()
            
            
            
            if let codeFrameView = codeFrameView {
                codeFrameView.layer.borderColor = UIColor.green.cgColor
                codeFrameView.layer.borderWidth = 2
                view.addSubview(codeFrameView)
                view.bringSubview(toFront: codeFrameView)
            }
            
            cancelButton = UIButton()
            cancelButton?.frame = CGRect(x: 0, y: self.view.frame.size.height-48, width: self.view.frame.size.width, height: 48)
            cancelButton?.backgroundColor = UIColor.red
            cancelButton?.setTitle("Cancel", for: UIControlState())
            cancelButton?.addTarget(self, action: #selector(ViewController.callScanCode(_:)), for: .touchUpInside)
            view.addSubview(cancelButton!)
            view.bringSubview(toFront: cancelButton!)
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
    
    func metadataOutput(_ captureOutput: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            codeFrameView?.frame = CGRect.zero
            print("No barcode is detected")
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {

            if metadataObj.stringValue != nil {

                let code = metadataObj.stringValue!
                
                if (self.charactersLongitude == code.characters.count){
                    
                    // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
                    let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
                    codeFrameView?.frame = barCodeObject!.bounds
                    
                    captureSession?.stopRunning()
                    let delayInSeconds = 1.0
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                        self.view.bringSubview(toFront: self.webView)
                        self.webView.evaluateJavaScript("sendBarCode('\(code)')", completionHandler: nil)
                    }
                }
            }
        }
    }
    
    @objc func callScanCode(_ sender: UIButton!){
        captureSession?.stopRunning()
        self.view.bringSubview(toFront: self.webView)
    }
    
    func startWebView(){
        let contentController = WKUserContentController();
        contentController.add(
            self,
            name: "callbackHandler"
        )
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        webView = WKWebView(frame: self.view.bounds, configuration: config)
        
        let requestURL = URL(string:"https://dev.pagamobil.com/paypal/categorias.php?locationId=FVKN7EJ5Y8292&customerId=N74KAYCQ74EZ4&tabId=X4SYY7QYKLK76")
        let request = URLRequest(url: requestURL!)
        webView.load(request)
        webView.isUserInteractionEnabled = true
        self.view.addSubview(webView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage)
    {
        
        let info = message.body as! Dictionary<String, Any>
        
        if (info["action"] as! String == "camera") {
            charactersLongitude = Int(info["lng"] as! String)!
            print(charactersLongitude)
            startCamera()
        } else if (info["action"] as! String == "recall"){
            charactersLongitude = info["lng"] as! Int
            let delayInSeconds = 1.0
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                self.view.layer.addSublayer(self.videoPreviewLayer!)
                self.codeFrameView?.frame = CGRect.zero
                self.view.bringSubview(toFront: self.codeFrameView!)
                self.captureSession?.startRunning()
            }
            
        }
    }


}

